import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // todo + showWinnerModal
  // call showModal function
  const title = ``;
  const bodyElement = `Winner ${fighter.name}`;
  const onClose = () => {
    document.location.reload();
  };
  showModal({ title, bodyElement, onClose });
}
