import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {

  if(fighter === undefined){
    return '';
  }

  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  const fighterImg = createFighterImage(fighter);

  const character = ['name', 'health', 'attack', 'defense'];
  const container = createElement({
    tagName: 'div',
    className: 'fighter-preview__info'
  });
  character.forEach(element => {
    let oneCharacter = createElement({
      tagName: 'p'
    });
    oneCharacter.innerText = `${element}: ${fighter[element]}`;
    container.append(oneCharacter);
  });

  position === 'right' ? fighterElement.append(fighterImg, container) : fighterElement.append(container, fighterImg);
  // todo: + createFighterPreview
  // show fighter info (image, name, health, etc.)
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
