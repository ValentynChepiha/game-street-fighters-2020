import { controls } from '../../constants/controls';
import { getDOMElement, setStyleAttribute } from '../helpers/domHelper';

const concatControls = pressedKeys => {
  const result = new Set();
  Object.keys(controls).forEach(key => {
    let oneControl = controls[key];

    if (Array.isArray(oneControl)) {
      let countControl = 0;
      for (let control of oneControl) {
        if (pressedKeys.has(control)) {
          countControl += 1;
        }
      }
      result.add(countControl === oneControl.length ? key : '');
    } else {
      if (pressedKeys.has(oneControl)) {
        result.add(key);
      }
    }
  });
  return result;
};

const showHealth = (healthBar, fighter, health) => {
  const { health: startHealth } = fighter;
  const newValue = health <= 0 ? '0%' : `${(health / startHealth) * 100}%`;
  setStyleAttribute(healthBar, 'width', newValue);
};

const calculateHealth = (healthFighter, numberFighter, actions, firstFighter, secondFighter, timeNow, timeFighter) => {
  const [hit, hitCritical] =
    numberFighter === 'first'
      ? ['PlayerOneAttack', 'PlayerOneCriticalHitCombination']
      : ['PlayerTwoAttack', 'PlayerTwoCriticalHitCombination'];
  let timePerson = timeFighter;
  let healthPerson = healthFighter;
  if (timeNow - timePerson >= 10000 && actions.has(hitCritical)) {
    timePerson = timeNow;
    healthPerson -= getCriticalDamage(firstFighter);
  } else if (actions.has(hit)) {
    healthPerson -= getDamage(firstFighter, secondFighter);
  }
  return [healthPerson, timePerson];
};

export async function fight(firstFighter, secondFighter) {
  return new Promise(resolve => {
    // todo: + fight
    // resolve the promise with the winner when fight is over
    let flagGameOver = false;
    const pressedKeys = new Set();

    const healthBarFirst = getDOMElement('#left-fighter-indicator');
    const healthBarSecond = getDOMElement('#right-fighter-indicator');

    let { health: healthFirst } = firstFighter;
    let { health: healthSecond } = secondFighter;
    let timeFirst = 0;
    let timeSecond = 0;

    const addControls = e => {
      pressedKeys.add(e.code);
    };

    const removeControls = e => {
      pressedKeys.delete(e.code);
    };

    const game = () => {
      const actions = concatControls(pressedKeys);

      if (!actions.has('PlayerOneBlock') && !actions.has('PlayerTwoBlock')) {
        let timeNow = performance.now();
        [healthSecond, timeFirst] = calculateHealth(
          healthSecond,
          'first',
          actions,
          firstFighter,
          secondFighter,
          timeNow,
          timeFirst
        );
        [healthFirst, timeSecond] = calculateHealth(
          healthFirst,
          'second',
          actions,
          secondFighter,
          firstFighter,
          timeNow,
          timeSecond
        );
      }

      showHealth(healthBarFirst, firstFighter, healthFirst);
      showHealth(healthBarSecond, secondFighter, healthSecond);

      if (healthFirst <= 0 || healthSecond <= 0) {
        flagGameOver = true;
      }

      if (flagGameOver) {
        pressedKeys.clear();
        document.removeEventListener('keydown', addControls);
        document.removeEventListener('keyup', removeControls);
        // todo: + do resolve
        resolve(healthFirst <= 0 ? secondFighter : firstFighter);
      } else {
        requestAnimationFrame(game);
      }
    };

    document.addEventListener('keydown', addControls);
    document.addEventListener('keyup', removeControls);
    requestAnimationFrame(game);
  });
}

export function getDamage(attacker, defender) {
  // todo: + getDamage
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // todo: + getHitPower
  // return hit power
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // todo: + getBlockPower
  // return block power
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;
  return defense * dodgeChance;
}

export function getCriticalDamage(attacker) {
  const { attack } = attacker;
  return 2 * attack;
}
